extern crate libc;
#[macro_use]
extern crate bitflags;

use libc::{c_ushort,c_int,c_ulong,sockaddr,sa_family_t};
use std::ffi::CString;
use std::io::{Error,Write,BufRead,ErrorKind};
use std::net::{Ipv4Addr};

extern "C" {
    fn ioctl(fd: c_int, req: c_ulong, ...) -> c_int;
}

bitflags! {
    pub flags IfFlags: c_ushort {
        const IFF_UP = 0x1,               /* Interface is up.  */
        const IFF_BROADCAST = 0x2,        /* Broadcast address valid.  */
        const IFF_DEBUG = 0x4,            /* Turn on debugging.  */
        const IFF_LOOPBACK = 0x8,         /* Is a loopback net.  */
        const IFF_POINTOPOINT = 0x10,     /* Interface is point-to-point link.  */
        const IFF_NOTRAILERS = 0x20,      /* Avoid use of trailers.  */
        const IFF_RUNNING = 0x40,         /* Resources allocated.  */
        const IFF_NOARP = 0x80,           /* No address resolution protocol.  */
        const IFF_PROMISC = 0x100,        /* Receive all packets.  */
        const IFF_ALLMULTI = 0x200,       /* Receive all multicast packets.  */
        const IFF_MASTER = 0x400,         /* Master of a load balancer.  */
        const IFF_SLAVE = 0x800,          /* Slave of a load balancer.  */
        const IFF_MULTICAST = 0x1000,     /* Supports multicast.  */
        const IFF_PORTSEL = 0x2000,       /* Can set media type.  */
        const IFF_AUTOMEDIA = 0x4000,     /* Auto media select active.  */
        const IFF_DYNAMIC = 0x8000,       /* Dialup device with changing addresses.  */
    }
}

const SIOCGIFNAME: c_ulong = 0x8910;
const SIOCGIFCONF: c_ulong = 0x8912;
const SIOCGIFFLAGS: c_ulong = 0x8913;
const SIOCSIFFLAGS: c_ulong = 0x8914;
const SIOCGIFADDR: c_ulong = 0x8915;
const SIOCSIFADDR: c_ulong = 0x8916;
const SIOCGIFDSTADDR: c_ulong = 0x8917;
const SIOCSIFDSTADDR: c_ulong = 0x8918;
const SIOCGIFBRDADDR: c_ulong = 0x8919;
const SIOCSIFBRDADDR: c_ulong = 0x891a;
const SIOCGIFNETMASK: c_ulong = 0x891b;
const SIOCSIFNETMASK: c_ulong = 0x891c;
const SIOCGIFMETRIC: c_ulong = 0x891d;
const SIOCSIFMETRIC: c_ulong = 0x891e;
const SIOCGIFMTU: c_ulong = 0x8921;
const SIOCSIFMTU: c_ulong = 0x8922;
const SIOCSIFHWADDR: c_ulong = 0x8924;
const SIOCGIFHWADDR: c_ulong = 0x8927;
const SIOCADDMULTI: c_ulong = 0x8931;
const SIOCDELMULTI: c_ulong = 0x8932;
const SIOCGIFINDEX: c_ulong = 0x8933;
const SIOCSIFHWBROADCAST: c_ulong = 0x8937;
const SIOCGIFTXQLEN: c_ulong = 0x8942;
const SIOCSIFTXQLEN: c_ulong = 0x8943;

const AF_INET: sa_family_t = 2;


#[repr(C)]
struct ifreq_flags {
    name: [u8; 16],
    flags: IfFlags,
}

pub fn get_flags(sock: c_int, dev: &str) -> Result<IfFlags, Error> {
    let mut req = ifreq_flags {
        name: [0; 16],
        flags: IfFlags::empty(),
    };
    try!(req.name.as_mut().write_all(dev.as_bytes()));
    match unsafe { ioctl(sock, SIOCGIFFLAGS, &mut req) } {
        -1 => Err(Error::last_os_error()),
        _ => Ok(req.flags)
    }
}

pub fn set_flags(sock: c_int, dev: &str, flags: &IfFlags) -> Result<(), Error> {
    let mut req = ifreq_flags {
        name: [0; 16],
        flags: flags.clone(),
    };
    try!(req.name.as_mut().write_all(dev.as_bytes()));
    match unsafe { ioctl(sock, SIOCSIFFLAGS, &mut req) } {
        -1 => Err(Error::last_os_error()),
        _ => Ok(())
    }
}

#[repr(C)]
struct ifreq_int {
    name: [u8; 16],
    value: c_int,
}

fn get_int_from_name(sock: c_int, dev: &str, ioctl_num: c_ulong) -> Result<c_int, Error> {
    let mut req = ifreq_int {
        name: [0; 16],
        value: 0,
    };
    try!(req.name.as_mut().write_all(dev.as_bytes()));
    match unsafe { ioctl(sock, ioctl_num, &mut req) } {
        -1 => Err(Error::last_os_error()),
        _ => Ok(req.value)
    }
}

fn set_int_from_name(sock: c_int, dev: &str, ioctl_num: c_ulong, value: c_int) -> Result<(), Error> {
    let mut req = ifreq_int {
        name: [0; 16],
        value: value,
    };
    try!(req.name.as_mut().write_all(dev.as_bytes()));
    match unsafe { ioctl(sock, ioctl_num, &mut req) } {
        -1 => Err(Error::last_os_error()),
        _ => Ok(())
    }
}

pub fn get_mtu(sock: c_int, dev: &str) -> Result<c_int, Error> {
    get_int_from_name(sock, dev, SIOCGIFMTU)
}

pub fn set_mtu(sock: c_int, dev: &str, mtu: c_int) -> Result<(), Error> {
    set_int_from_name(sock, dev, SIOCSIFMTU, mtu)
}

pub fn get_qlen(sock: c_int, dev: &str) -> Result<c_int, Error> {
    get_int_from_name(sock, dev, SIOCGIFTXQLEN)
}

pub fn set_qlen(sock: c_int, dev: &str, qlen: c_int) -> Result<(), Error> {
    set_int_from_name(sock, dev, SIOCSIFTXQLEN, qlen)
}

pub fn get_name(sock: c_int, index: c_int) -> Result<CString, Error> {
    let mut req = ifreq_int {
        name: [0; 16],
        value: index,
    };
    match unsafe { ioctl(sock, SIOCGIFNAME, &mut req) } {
        -1 => Err(Error::last_os_error()),
        _ => {
            let mut vec_name: Vec<u8> = Vec::new();
            try!(req.name.as_ref().read_until(0, &mut vec_name));
            vec_name.pop();
            Ok(CString::new(vec_name).unwrap())
        }
    }
}

pub fn get_index(sock: c_int, dev: &str) -> Result<c_int, Error> {
    get_int_from_name(sock, dev, SIOCGIFINDEX)
}

pub fn get_metric(sock: c_int, dev: &str) -> Result<c_int, Error> {
    get_int_from_name(sock, dev, SIOCGIFMETRIC)
}

pub fn set_metric(sock: c_int, dev: &str, metric: c_int) -> Result<(), Error> {
    set_int_from_name(sock, dev, SIOCSIFMETRIC, metric)
}

#[repr(C)]
struct ifreq_address {
    name: [u8; 16],
    value: sockaddr,
}

fn get_address_from_name(sock: c_int, dev: &str, ioctl_num: c_ulong) -> Result<sockaddr, Error> {
    let mut req = ifreq_address {
        name: [0; 16],
        value: sockaddr { sa_family: 0, sa_data: [0; 14] },
    };
    try!(req.name.as_mut().write_all(dev.as_bytes()));
    match unsafe { ioctl(sock, ioctl_num, &mut req) } {
        -1 => Err(Error::last_os_error()),
        _ => Ok(req.value)
    }
}

fn set_address_from_name(sock: c_int, dev: &str, ioctl_num: c_ulong, value: sockaddr) -> Result<(), Error> {
    let mut req = ifreq_address {
        name: [0; 16],
        value: value,
    };
    try!(req.name.as_mut().write_all(dev.as_bytes()));
    match unsafe { ioctl(sock, ioctl_num, &mut req) } {
        -1 => Err(Error::last_os_error()),
        _ => Ok(())
    }
}

pub fn get_address(sock: c_int, dev: &str) -> Result<sockaddr, Error> {
    get_address_from_name(sock, dev, SIOCGIFADDR)
}

pub fn set_address(sock: c_int, dev: &str, address: sockaddr) -> Result<(), Error> {
    set_address_from_name(sock, dev, SIOCSIFADDR, address)
}

pub fn get_netmask(sock: c_int, dev: &str) -> Result<sockaddr, Error> {
    get_address_from_name(sock, dev, SIOCGIFNETMASK)
}

pub fn set_netmask(sock: c_int, dev: &str, address: sockaddr) -> Result<(), Error> {
    set_address_from_name(sock, dev, SIOCSIFNETMASK, address)
}

pub fn get_destination(sock: c_int, dev: &str) -> Result<sockaddr, Error> {
    get_address_from_name(sock, dev, SIOCGIFDSTADDR)
}

pub fn set_destination(sock: c_int, dev: &str, address: sockaddr) -> Result<(), Error> {
    set_address_from_name(sock, dev, SIOCSIFDSTADDR, address)
}

pub fn get_broadcast(sock: c_int, dev: &str) -> Result<sockaddr, Error> {
    get_address_from_name(sock, dev, SIOCGIFBRDADDR)
}

pub fn set_broadcast(sock: c_int, dev: &str, address: sockaddr) -> Result<(), Error> {
    set_address_from_name(sock, dev, SIOCSIFBRDADDR, address)
}

pub fn get_hardware(sock: c_int, dev: &str) -> Result<sockaddr, Error> {
    get_address_from_name(sock, dev, SIOCGIFHWADDR)
}

pub fn set_hardware(sock: c_int, dev: &str, address: sockaddr) -> Result<(), Error> {
    set_address_from_name(sock, dev, SIOCSIFHWADDR, address)
}

pub fn set_hardware_broadcast(sock: c_int, dev: &str, address: sockaddr) -> Result<(), Error> {
    set_address_from_name(sock, dev, SIOCSIFHWBROADCAST, address)
}

pub fn add_address_multi(sock: c_int, dev: &str, address: sockaddr) -> Result<(), Error> {
    set_address_from_name(sock, dev, SIOCADDMULTI, address)
}

pub fn del_address_multi(sock: c_int, dev: &str, address: sockaddr) -> Result<(), Error> {
    set_address_from_name(sock, dev, SIOCDELMULTI, address)
}

#[repr(C)]
struct ifreq_padded {
    name: [u8; 16],
    value: sockaddr,
    padding: [u8; 8],
}

#[repr(C)]
struct ifconf {
    buf_len: c_int,
    buf_ptr: *mut ifreq_padded,
}

fn cstr_from_name(name: &mut [u8]) -> CString {
    let mut vec_name: Vec<u8> = Vec::new();
    name.as_ref().read_until(0, &mut vec_name).unwrap();
    vec_name.pop();
    CString::new(vec_name).unwrap()
}

pub fn get_addresses(sock: c_int) -> Result<Vec<(CString, sockaddr)>, Error> {
    let mut req = ifconf {
        buf_len: 0,
        buf_ptr: std::ptr::null_mut(),
    };
    try!(match unsafe { ioctl(sock, SIOCGIFCONF, &mut req) } {
        -1 => Err(Error::last_os_error()),
        _ => Ok(())
    });

    let ifreq_padded_size = 40;
    let answers_available = req.buf_len / ifreq_padded_size;

    let mut ifaces: Vec<ifreq_padded> = Vec::with_capacity(answers_available as usize);
    let mut req2 = ifconf {
        buf_len: req.buf_len,
        buf_ptr: ifaces.as_mut_ptr(),
    };
    try!(match unsafe { ioctl(sock, SIOCGIFCONF, &mut req2) } {
        -1 => Err(Error::last_os_error()),
        _ => Ok(())
    });
    let items_transferred = req2.buf_len/ifreq_padded_size;
    unsafe { ifaces.set_len(items_transferred as usize) };

    Ok(ifaces.into_iter().map(|mut iface| (cstr_from_name(iface.name.as_mut()), iface.value.clone())).collect())
}

pub fn sockaddr_to_ipaddr(sa: &sockaddr) -> Result<Ipv4Addr, Error> {
    match sa.sa_family {
        AF_INET => Ok(Ipv4Addr::new( sa.sa_data[2] as u8, sa.sa_data[3] as u8, sa.sa_data[4] as u8, sa.sa_data[5] as u8)),
        _ => Err(Error::new(ErrorKind::Other, "unknown family")),
    }
}
