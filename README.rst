=========
Netdevice
=========

Rust interface to linux netdevice ioctls. Unless you specifically want netdevice
access, you probably don't want this crate. (legacy interface)
