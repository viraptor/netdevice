extern crate libc;
extern crate netdevice;
use netdevice::{get_flags,set_flags,get_mtu};
use netdevice::{IFF_PROMISC};
use libc::{c_int,ssize_t,size_t};
use std::io::Error;

extern "C" {
    fn htons(hostshort: u16) -> u16;
//    fn htonl(hostlong: u32) -> u32;
    fn read(fildes: c_int, buf: *mut u8, nbyte: size_t) -> ssize_t;
}

const ETH_P_ALL: u16 = 3;
const PF_SOCKET: c_int = 17;
const SOCK_RAW: c_int = 3;

fn sock_read(sock: c_int, buf: &mut [u8]) -> Result<ssize_t, Error> {
    match unsafe { read(sock, buf.as_mut_ptr(), buf.len()) } {
        -1 => Err(Error::last_os_error()),
        res => Ok(res)
    }
}

fn set_promisc(sock: c_int, dev: &str) {
    let mut flags = get_flags(sock, dev).unwrap();
    if !flags.contains(IFF_PROMISC) {
        flags.insert(IFF_PROMISC);
        set_flags(sock, dev, &flags).unwrap();
    }
}

fn unset_promisc(sock: c_int, dev: &str) {
    let mut flags = get_flags(sock, dev).unwrap();
    if flags.contains(IFF_PROMISC) {
        flags.remove(IFF_PROMISC);
        set_flags(sock, dev, &flags).unwrap();
    }
}

fn dump_packets(sock: c_int, mtu: c_int) {
    let mut buf: Vec<u8> = Vec::new();
    buf.resize(mtu as usize, 0);
    loop {
        let len = sock_read(sock, buf.as_mut()).unwrap();
        println!("new packet: len {}", len);
        println!("content: {:?}", buf);
    }
}

fn get_raw_socket() -> Result<c_int, Error> {
    let res = unsafe { libc::socket(PF_SOCKET, SOCK_RAW, htons(ETH_P_ALL) as c_int) };
    match res{
        -1 => Err(Error::last_os_error()),
        sock => Ok(sock)
    }
}

fn main() {
    let sock = get_raw_socket().unwrap();
    println!("got socket: {}", sock);

    let dev = "wlp4s0";
    let mtu = get_mtu(sock, &dev).unwrap();
    println!("got mtu: {}", mtu);
    set_promisc(sock, &dev);

    dump_packets(sock, mtu);

    unset_promisc(sock, &dev);
}
