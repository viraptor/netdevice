extern crate libc;
extern crate netdevice;
use std::io::Error;
use libc::{c_int};

const ETH_P_ALL: u16 = 3;
const PF_SOCKET: c_int = 17;
const SOCK_RAW: c_int = 3;

extern "C" {
    fn htons(hostshort: u16) -> u16;
}

fn get_raw_socket() -> Result<c_int, Error> {
    let res = unsafe { libc::socket(PF_SOCKET, SOCK_RAW, htons(ETH_P_ALL) as c_int) };
    match res{
        -1 => Err(Error::last_os_error()),
        sock => Ok(sock)
    }
}

fn main() {
    let sock = get_raw_socket().unwrap();
    println!("got socket: {}", sock);
    let lo = netdevice::get_index(sock, "lo").unwrap();
    println!("lo number: {}", lo);
    let name = netdevice::get_name(sock, lo).unwrap();
    println!("name: {:?}", name);

    for (name, sa) in netdevice::get_addresses(sock).unwrap() {
        println!("name: {:?} -> {}", name, netdevice::sockaddr_to_ipaddr(&sa).unwrap());
    }
}
